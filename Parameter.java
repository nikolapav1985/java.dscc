/**
*
* class Parameter
*
* example of using formal and actual parameters
*
* compile ----- javac Parameter.java
*
* run ----- java Parameter -name-
*
* test environment ----- os lubuntu 16.04 javac version 1.8.0
*
* example output ----- Hello T800!
*
*/
class Parameter{
    /**
    *
    * method hello
    *
    * parameters
    *
    * name (String) - a name to print
    *
    * formal parameters
    *
    * a specification (or list) of values that need to be passed to a function or method
    *
    */
    public static void hello(String name){
        System.out.println("Hello "+name+"!");
    }
    public static void main(String[] args){
        if(args.length != 1){ // needs one command line parameter
            // if no command line parameter, print message, exit, enable error code
            System.out.println("Usage java Parameter -name-");
            System.exit(1);
        }
        hello(args[0]); // call a static method using a parameter (given via command line)
    }
}
